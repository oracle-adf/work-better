# Work-Better
Oracle Alta UI Work Better Application Demo - 12.2.1

App Example:  
http://jdevadf.oracle.com/workbetter/faces/index

The Oracle Alta UI system was used to develop the most recent Oracle Cloud products, the latest versions of Oracle Fusion Applications, a number of innovative mobile applications, with many more to come. The Alta UI provides a fully redesigned UI component set and interactions, a fresh visual design that complements a modern layout approach, a UI that is conducive to responsive design techniques, and a native mobile UI. This demo, Oracle Alta UI Work Better Application, consists of an application workspace that can be opened in JDeveloper, explored, and deployed to WebLogic, integrated or standalone, application server.

Download Link:  
http://www.oracle.com/technetwork/developer-tools/jdev/learnmore/index-098948.html
